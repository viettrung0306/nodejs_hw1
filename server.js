const express = require("express");
const morgan = require("morgan");
const path = require("path");
const fs = require("fs");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();
const directoryPath = path.join(__dirname, "created_files");
const port = 8080;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
app.use(morgan("tiny", { stream: accessLogStream }));

app.post("/api/files", (req, res) => {
  try {
    console.log(req.body);
    const { filename, content } = req.body;
    const FILE_PATH = `./created_files/${filename}`;
    console.log(filename);
    if (!filename || !content) {
      let message = !filename
        ? "Please specify 'filename' parameter"
        : "Please specify 'content' parameter";
      res.status(400).json({ message: message });
    } else {
      fs.appendFile(FILE_PATH, content, (err) => {
        if (err) throw res.status(500).json({ message: "Server error" });
        res.status(200).json({ message: "File created successfully" });
      });
    }
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
});

app.get("/api/files", (req, res) => {
  console.log(directoryPath);
  try {
    if (directoryPath === undefined) {
      res.status(400).json({ message: "Client error" });
    } else {
      fs.readdir(directoryPath, (err, files) => {
        if (err) throw res.status(400).json({ message: "Server error" });
        res.status(200).json({ message: "Success", files: files });
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Server error" });
  }
});

app.get("/api/files/:filename", (req, res) => {
  const filename = req.params.filename;

  try {
    const { birthtime } = fs.statSync(directoryPath + "/" + filename);
    const filePath = directoryPath + "/" + filename;
    !filePath &&
      res
        .status(400)
        .json({ message: `No file with '${filename}' filename found.` });
    fs.readFile(filePath, "utf8", (err, data) => {
      if (err) {
        console.error(err);
        res.status(500).json({ message: "Server error" });
      }

      let ext = path.extname(filename || "").split(".");
      let resObj = {
        message: "Success",
        filename: filename,
        content: data,
        extension: ext[ext.length - 1],
        uploadedDate: birthtime,
      };

      res.status(200).json(resObj);
    });
  } catch (error) {
    res
      .status(400)
      .json({ message: `No file with '${filename}' filename found.` });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
